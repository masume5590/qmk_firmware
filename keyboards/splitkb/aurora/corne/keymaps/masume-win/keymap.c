#include QMK_KEYBOARD_H
#include "keymap_german.h"

enum {
    BASE,
    NUM,
    SYM,
    DEL,
    UNDO,
    MOVE,
    MISC
};

// TAP DANCE

enum {
    TD_ALT,
    TD_CTL,
    TD_SFT,
    TD_GUI,
};

// CUSTOM KEYCODES
enum {
    MV_WORD_LEFT = SAFE_RANGE,
    MV_WORD_RIGHT,
    MV_LINE_START,
    MV_LINE_END,
    DEL_WORD_LEFT,
    DEL_WORD_RIGHT,
    DEL_LINE_START,
    DEL_LINE_END,
};


// Define a type containing as many tapdance states as you need
typedef enum { TD_NONE, TD_UNKNOWN, TD_SINGLE_TAP, TD_SINGLE_HOLD, TD_DOUBLE_SINGLE_TAP } td_state_t;

// Create a global instance of the tapdance state type
static td_state_t td_state;

// Declare your tapdance functions:

// Function to determine the current tapdance state
td_state_t cur_dance(qk_tap_dance_state_t *state);

// `finished` and `reset` functions for each tapdance keycode
void altlp_finished(qk_tap_dance_state_t *state, void *user_data);
void altlp_reset(qk_tap_dance_state_t *state, void *user_data);

// clang-format off8
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
[BASE] = LAYOUT_split_3x6_3(
   KC_TAB , DE_Q          ,DE_W,DE_F         ,DE_P           , DE_B ,                   DE_J ,DE_L ,DE_U   ,DE_Y  ,KC_SLSH   ,KC_BSPC   ,
   KC_LSFT, DE_A          ,DE_R,LT(DEL, DE_S),LT(MOVE, DE_T) , DE_G ,                   DE_M ,DE_N ,DE_E   ,DE_I  ,DE_O, DE_QUOT,
   KC_LCTL, LT(UNDO, DE_Z),KC_X,KC_C         ,KC_D           , KC_V ,                   KC_K ,KC_H ,KC_COMM,KC_DOT,KC_AMPR, KC_RALT,
                                           KC_LCTL, KC_ENT, KC_LALT,KC_LALT, LT(NUM, KC_SPC),     LT(SYM, KC_ENT),KC_RSFT,KC_RALT, KC_LGUI, LT(DEL, KC_BSPC)
 ),
[NUM] =  LAYOUT_split_3x6_3 (
    KC_ESC,     KC_1,    KC_2,    KC_3,    KC_4,    KC_5,                       KC_6,       KC_7,    KC_8,    KC_9,    KC_0, KC_BSPC,
    _______, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                       KC_LEFT, KC_DOWN,   KC_UP,KC_RIGHT, XXXXXXX, XXXXXXX,
    _______, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                       XXXXXXX, XXXXXXX, KC_COMM,KC_DOT, XXXXXXX, XXXXXXX,
                                        _______, _______,_______, _______, _______,       MO(MISC), _______, _______, _______, _______
),
[SYM] = LAYOUT_split_3x6_3(
    KC_ESC, DE_EXLM,    DE_AT, DE_HASH,  DE_DLR, DE_PERC,                       DE_CIRC, DE_AMPR, DE_ASTR, DE_LPRN, DE_RPRN, KC_BSPC,
    _______, XXXXXXX, DE_LPRN, DE_RPRN, DE_DQUO, XXXXXXX,                       DE_PLUS,  DE_EQL, DE_LBRC, DE_RBRC, DE_BSLS,  DE_GRV,
    _______, XXXXXXX, XXXXXXX, DE_LABK, DE_RABK, DE_RABK,                       DE_MINS, DE_LCBR, DE_LCBR, DE_RCBR,  DE_PIPE, DE_TILD,
                                        _______, _______,_______, _______, MO(MISC),         KC_ENT, XXXXXXX, _______, _______, _______
),
[DEL] = LAYOUT_split_3x6_3(
  _______,_______,_______,_______,_______,_______,                     _______,_______,_______,_______,_______,_______,
  _______,_______,_______,_______,_______,_______,                     DEL_LINE_START, DEL_WORD_LEFT, KC_BSPC,KC_DEL, DEL_WORD_RIGHT, DEL_LINE_END,
  _______,_______,_______,_______,_______,_______,                     _______,_______,_______,_______,_______,_______,
                                  _______, _______,_______,_______,_______,     _______,_______,_______, _______, _______
),
[UNDO] = LAYOUT_split_3x6_3(
  _______,_______,_______,_______,_______,_______,                     _______,_______,_______,_______,_______,_______,
  _______,_______,_______,_______,_______,_______,                     _______,_______,_______,_______,_______,_______,
  _______,_______,_______,_______,_______,_______,                     _______,_______,_______,_______,_______,_______,
                                  _______, _______,_______,_______,_______,     _______,_______,_______, _______, _______
),
[MOVE] = LAYOUT_split_3x6_3(
  _______,_______,_______,_______,_______,_______,                     _______,_______,_______,_______,_______,_______,
  _______,_______,_______,_______,_______,_______,                     MV_LINE_START, MV_WORD_LEFT, KC_LEFT,KC_RIGHT, MV_WORD_RIGHT, MV_LINE_END,
  _______,_______,_______,_______,_______,_______,                     _______,_______,_______,_______,_______,_______,
                                  _______, _______,_______,_______,_______,     _______,_______,_______, _______, _______
),
[MISC] = LAYOUT_split_3x6_3(
  _______,_______,_______,_______,_______,_______,                     _______,_______,_______,_______,_______,_______,
  _______,_______,_______,_______,_______,_______,                     _______,_______,_______,_______,_______,_______,
  _______,_______,_______,_______,_______,_______,                     _______,_______,_______,_______,_______,_______,
                                  _______,_______,MAGIC_UNSWAP_CTL_GUI,_______,_______,_______,_______,MAGIC_SWAP_CTL_GUI, _______, _______
),
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case MV_LINE_START:
            if (record->event.pressed) {
                tap_code(KC_HOME);
            }
            break;
        case MV_WORD_LEFT:
            if (record->event.pressed) {
                register_code(KC_LCTL);
                tap_code(KC_LEFT);
                unregister_code(KC_LCTL);
            }
            break;
        case MV_WORD_RIGHT:
            if (record->event.pressed) {
                register_code(KC_LCTL);
                tap_code(KC_RIGHT);
                unregister_code(KC_LCTL);
            }
            break;
        case MV_LINE_END:
            if (record->event.pressed) {
                tap_code(KC_END);
            }
            break;
        case DEL_LINE_START:
            if (record->event.pressed) {
                register_code(KC_LSFT);
                tap_code(KC_HOME);
                unregister_code(KC_LSFT);
                tap_code(KC_DEL);
            }
            break;
        case DEL_WORD_LEFT:
            if (record->event.pressed) {
                register_code(KC_LCTL);
                tap_code(KC_BSPC);
                unregister_code(KC_LCTL);
            }
            break;
        case DEL_WORD_RIGHT:
            if (record->event.pressed) {
                register_code(KC_LCTL);
                tap_code(KC_DEL);
                unregister_code(KC_LCTL);
            }
            break;
        case DEL_LINE_END:
            if (record->event.pressed) {
                register_code(KC_LSFT);
                tap_code(KC_END);
                unregister_code(KC_LSFT);
                tap_code(KC_DEL);
            }
            break;
    }
    return true;
}

bool encoder_update_user(uint8_t index, bool clockwise) {
    // 0 is left-half encoder,
    // 1 is right-half encoder
    // on the DEL layer, encoders tap delete and backspace
    if (get_mods() & ( MOD_BIT(KC_LCTL))) {
      if (clockwise) {
        tap_code(KC_TAB);
    } else {
        register_code(KC_LSFT);
        tap_code(KC_TAB);
        unregister_code(KC_LSFT);
    }
} else if (IS_LAYER_ON(DEL)) {
    if (index == 0) {
        if (clockwise) {
            tap_code(KC_DEL);
        } else {
            tap_code(KC_BSPC);
        }
    }
    if (index == 1) {
        if (clockwise) {
                // Delete line below
            tap_code(KC_DOWN);
            tap_code(KC_END);
            register_code(KC_LGUI);
            tap_code(KC_BSPC);
            unregister_code(KC_LGUI);
            tap_code(KC_BSPC);
        } else {
                // Delete line above
            tap_code(KC_UP);
            tap_code(KC_END);
            register_code(KC_LGUI);
            tap_code(KC_BSPC);
            unregister_code(KC_LGUI);
            tap_code(KC_BSPC);
            tap_code(KC_DOWN);
        }
    }
} else if (IS_LAYER_ON(UNDO)) {
    if (clockwise) {
            // Redo
        register_code(KC_LCTL);
        register_code(KC_LSFT);
        tap_code(DE_Z);
        unregister_code(KC_LSFT);
        unregister_code(KC_LCTL);
    } else {
            // Undo
        register_code(KC_LCTL);
        tap_code(DE_Z);
        unregister_code(KC_LCTL);

    }
} else if (IS_LAYER_ON(MOVE)) {
    if (index == 0) {
        if (clockwise) {
            tap_code(KC_MS_WH_RIGHT);
        } else {
            tap_code(KC_MS_WH_LEFT);
        }
    }
    if (index == 1) {
        if (clockwise) {
            tap_code(KC_MS_WH_UP);
        } else {
            tap_code(KC_MS_WH_DOWN);
        }
    }
} else {
    if (index == 0) {
        if (clockwise) {
            tap_code(KC_RIGHT);
        } else {
            tap_code(KC_LEFT);
        }
    } else if (index == 1) {
        if (clockwise) {
            tap_code(KC_DOWN);
        } else {
            tap_code(KC_UP);
        }
    }
}
return false;
}

// COMBOS

enum combos {
    AR_ADIA,
    RS_SS,
    UY_UDIA,
    OI_ODIA,
};

const uint16_t PROGMEM ar_combo[] = {DE_A, DE_R, COMBO_END};
const uint16_t PROGMEM rs_combo[] = {DE_R, LT(DEL, DE_S), COMBO_END};
const uint16_t PROGMEM uy_combo[] = {DE_U, DE_Y, COMBO_END};
const uint16_t PROGMEM io_combo[] = {DE_O, DE_I, COMBO_END};

combo_t key_combos[COMBO_COUNT] = {
    [AR_ADIA] = COMBO(ar_combo, DE_ADIA),
    [RS_SS]   = COMBO(rs_combo, DE_SS),
    [UY_UDIA] = COMBO(uy_combo, DE_UDIA),
    [OI_ODIA] = COMBO(io_combo, DE_ODIA),
};

const key_override_t qmrk_key_override = ko_make_basic(MOD_MASK_SHIFT, KC_UNDS, KC_EXLM);
const key_override_t slsh_key_override = ko_make_basic(MOD_MASK_SHIFT, DE_SLSH, KC_UNDS);
const key_override_t paren_key_override = ko_make_basic(MOD_MASK_SHIFT, DE_LPRN,DE_RPRN);
const key_override_t bracket_key_override = ko_make_basic(MOD_MASK_SHIFT, DE_LBRC,DE_RBRC);
const key_override_t brace_key_override = ko_make_basic(MOD_MASK_SHIFT, DE_LCBR,DE_RCBR);
const key_override_t quot_key_override = ko_make_basic(MOD_MASK_SHIFT, DE_QUOT,DE_DQUO);

// This globally defines all key overrides to be used
const key_override_t **key_overrides = (const key_override_t *[]){
    &qmrk_key_override,
    &slsh_key_override,
    &paren_key_override,
    &bracket_key_override,
    &brace_key_override,
    &quot_key_override,
    NULL // Null terminate the array of overrides!
};


// TAP DANCE

// Determine the tapdance state to return
td_state_t cur_dance(qk_tap_dance_state_t *state) {
    if (state->count == 1) {
        if (!state->pressed) return TD_SINGLE_TAP;
        else return TD_SINGLE_HOLD;
    }

    if (state->count == 2) return TD_DOUBLE_SINGLE_TAP;
    else return TD_UNKNOWN; // Any number higher than the maximum state value you return above
}

// Handle the possible states for each tapdance keycode you define:

void modtd_finished(qk_tap_dance_state_t *state, void *user_data, int bare_tap_key, int shited_tap_key, int mod_key) {
    int tap_key = (get_mods() & MOD_BIT(KC_LSFT)) ? shited_tap_key : bare_tap_key;
    td_state = cur_dance(state);
    switch (td_state) {
        case TD_SINGLE_TAP:
            register_code16(tap_key);
            break;
        case TD_SINGLE_HOLD:
            register_mods(MOD_BIT(mod_key)); // For a layer-tap key, use `layer_on(_MY_LAYER)` here
            break;
        case TD_DOUBLE_SINGLE_TAP: // Allow nesting of 2 parens `((` within tapping term
            tap_code16(tap_key);
            register_code16(tap_key);
            break;
        default:
            break;
    }
}


void modtd_reset(qk_tap_dance_state_t *state, void *user_data, int bare_tap_key, int shited_tap_key, int mod_key) {
        int tap_key = (get_mods() & MOD_BIT(KC_LSFT)) ? shited_tap_key : bare_tap_key;
    switch (td_state) {
        case TD_SINGLE_TAP:
            unregister_code16(tap_key);
            break;
        case TD_SINGLE_HOLD:
            unregister_mods(MOD_BIT(mod_key)); // For a layer-tap key, use `layer_off(_MY_LAYER)` here
            unregister_mods(MOD_BIT(KC_LSFT));
            break;
        case TD_DOUBLE_SINGLE_TAP:
            unregister_code16(tap_key);
            break;
        default:
            break;
    }
}

void td_alt_finished(qk_tap_dance_state_t *state, void *user_data) {
    modtd_finished(state, user_data, DE_QUOT, DE_DQUO, KC_LALT);
}

void td_alt_reset(qk_tap_dance_state_t *state, void *user_data) {
    modtd_reset(state, user_data, DE_QUOT, DE_DQUO, KC_LALT);
}

void td_ctl_finished(qk_tap_dance_state_t *state, void *user_data) {
    modtd_finished(state, user_data, DE_COLN, DE_SCLN, KC_LCTL);
}

void td_ctl_reset(qk_tap_dance_state_t *state, void *user_data) {
    modtd_reset(state, user_data, DE_COLN, DE_SCLN, KC_LCTL);
}

void td_sft_finished(qk_tap_dance_state_t *state, void *user_data) {
    modtd_finished(state, user_data, DE_EQL, DE_EQL, KC_LSFT);
}

void td_sft_reset(qk_tap_dance_state_t *state, void *user_data) {
    modtd_reset(state, user_data, DE_EQL, DE_EQL, KC_LSFT);
}

void td_gui_finished(qk_tap_dance_state_t *state, void *user_data) {
    modtd_finished(state, user_data, DE_TILD, DE_TILD, KC_LGUI);
}

void td_gui_reset(qk_tap_dance_state_t *state, void *user_data) {
    modtd_reset(state, user_data, DE_TILD, DE_TILD, KC_LGUI);
}

// Define `ACTION_TAP_DANCE_FN_ADVANCED()` for each tapdance keycode, passing in `finished` and `reset` functions
qk_tap_dance_action_t tap_dance_actions[] = {
    [TD_ALT] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, td_alt_finished, td_alt_reset),
    [TD_CTL] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, td_ctl_finished, td_ctl_reset),
    [TD_SFT] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, td_sft_finished, td_sft_reset),
    [TD_GUI] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, td_gui_finished, td_gui_reset),
};
