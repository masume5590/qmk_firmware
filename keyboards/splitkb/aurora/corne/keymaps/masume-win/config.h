/* Copyright 2022 splitkb.com <support@splitkb.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#define COMBO_COUNT 4
#define SPLIT_LAYER_STATE_ENABLE

#define RGB_LED_NUM 48

// clang-format off
#define RGB_LAYOUT( \
    L00, L01, L02, L03, L04, L05,             R06, R07, R08, R09, R10, R11, \
    L12, L13, L14, L15, L16, L17,             R18, R19, R20, R21, R22, R23, \
    L24, L25, L26, L27, L28, L29,             R34, R35, R36, R37, R38, R39, \
                        L41, L42, L43,   R46, R47, R48  \
) \
{ \
    L43, L42, L41, L29, L28, L27, L26, L25, L17, L16, L15, L14, L13, L05, L04, L03, L02, L01, L00, L12, L24, \
    KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,  \
    R46, R47, R48, R34, R35, R36, R37, R38, R18, R19, R20, R21, R22, R06, R07, R08, R09, R10, R11, R23, R39, \
}
#define RAW_USAGE_PAGE 0xFF60
#define RAW_USAGE_ID 0x61

// TAP DANCE

#define TAPPING_TERM 150

#define COMBO_TERM 20
