const MAIN_ROWS = 3;
const MAIN_COLUMNS = 6;
const THUMB_ROWS = 1;
const THUMB_COLUMNS = 3;
const THUMB_OFFSET = 4;
const THUMB_OVERHANG = THUMB_COLUMNS + THUMB_OFFSET - MAIN_COLUMNS;
const SPLIT_OFFSET = 5;

function main() {
  const formattedKeymap = formatKeymap(Deno.readTextFileSync("keymap.c"));

  Deno.writeTextFileSync("keymap.c", formattedKeymap);
}

main();

function formatKeymap(keymaptext: string) {
  for (let i = 0; i < keymaptext.length; i++) {
    if (
      keymaptext[i] === "(" && keymaptext[i - 1] === "T" &&
      keymaptext[i - 2] === "U" && keymaptext[i - 3] === "O" &&
      keymaptext[i - 4] === "Y" && keymaptext[i - 5] === "A" &&
      keymaptext[i - 6] === "L"
    ) {
      const start = i;
      let end = i;
      let parenDepth = 1;
      for (let j = i + 1; j < keymaptext.length; j++) {
        if (keymaptext[j] === "(") {
          parenDepth++;
        } else if (keymaptext[j] === ")") {
          parenDepth--;
        }
        if (parenDepth === 0) {
          end = j;
          break;
        }
      }
      const layout = keymaptext.slice(start, end + 1);
      const layoutSingleLine = layout.replace(/\n/g, "");
      const layoutSingleLineSingleSpace = layoutSingleLine.replace(/\s+/g, " ");
      keymaptext = keymaptext.slice(0, start) + layoutSingleLineSingleSpace +
        keymaptext.slice(end + 1);
    }
  }

  const keymapLines = keymaptext.split("\n");
  const resultLines = [];
  for (const line of keymapLines) {
    if (line.includes("LAYOUT")) {
      resultLines.push(formatLayout(line));
    } else {
      resultLines.push(line);
    }
  }
  return resultLines.join("\n");
}

function formatLayout(line: string) {
  const indentation = line.match(/^\s*/)![0].length + 2;
  const columns = getKeyColumns(line);
  const resultLines = [];
  const lines = range(0, MAIN_ROWS + THUMB_ROWS).map((i) => {
    return renderRow(columns, i);
  });
  lines[lines.length - 1] = lines[lines.length - 1].trimEnd().slice(0, -1);
  const linesIndented = lines.map((line) => {
    return " ".repeat(indentation) + line;
  });
  const beforeLayout = line.match(/^.*LAYOUT\(/)![0];
  resultLines.push(beforeLayout);
  resultLines.push(...linesIndented);
  resultLines.push(" ".repeat(indentation - 2) + "),");
  return resultLines.join("\n");
}

function renderRow(
  columns: Array<Array<string | undefined>>,
  row: number,
) {
  let result = "";
  const widths = columns.map((col) => getWidth(col));
  //   widths[MAIN_COLUMNS + THUMB_OVERHANG] += SPLIT_OFFSET;
  for (let col = 0; col < columns.length; col++) {
    const key = columns[col][row];
    console.log(`col: ${col}, row: ${row}, key: ${key}`);
    if (col === MAIN_COLUMNS + THUMB_OVERHANG) {
      result += " ".repeat(SPLIT_OFFSET);
    }
    if (key === undefined) {
      result += " ".repeat(widths[col] + 1);
    } else {
      result += key.padEnd(widths[col]) + ",";
    }
  }
  return result;
}

function getWidth(col: Array<string | undefined>) {
  return col.reduce((acc, key) => {
    return Math.max(acc, key?.length ?? 0);
  }, 0);
}

function getKeyColumns(line: string) {
  const inBrackets = line.match(/\(.*\)/)![0];
  const inBracketsWithoutParen = inBrackets.slice(1, inBrackets.length - 1);
  // replace commas inside parentheses
  const inBracketsWithoutParenCommas = inBracketsWithoutParen.replace(
    /\([^)]*\)/g,
    (match) => match.replace(/,/g, "COMMA"),
  );
  const keys = inBracketsWithoutParenCommas.split(",").map((key) =>
    key.replace("COMMA", ",").trim()
  );
  const columns: Array<Array<string | undefined>> = [];
  const getCol = (i: number) => {
    if (columns[i] === undefined) {
      columns[i] = [];
    }
    return columns[i];
  };
  for (let row = 0; row < MAIN_ROWS; row++) {
    for (let col = 0; col < MAIN_COLUMNS; col++) {
      const key = keys.shift();
      getCol(col)[row] = key;
    }
    for (let col = 0; col < MAIN_COLUMNS; col++) {
      const key = keys.shift();
      getCol(MAIN_COLUMNS + 2 * THUMB_OVERHANG + col)[row] = key;
    }
  }
  for (let row = 0; row < THUMB_ROWS; row++) {
    for (let col = 0; col < THUMB_COLUMNS; col++) {
      const key = keys.shift();
      getCol(THUMB_OFFSET + col)[row + MAIN_ROWS] = key;
    }
    for (let col = 0; col < THUMB_COLUMNS; col++) {
      const key = keys.shift();
      getCol(THUMB_OVERHANG + MAIN_COLUMNS + col)[row + MAIN_ROWS] = key;
    }
  }
  return columns;
}

function range(start: number, end: number) {
  return Array.from({ length: end - start }, (_, i) => i + start);
}
